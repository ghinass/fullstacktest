<?php
class Login extends CI_Controller {
	public function __construct()
	{
			parent::__construct();
			$this->load->model('utils/Utils');
			
			 
	}
	public function index()
	{
		 
	}
	public function authenticate($email='',$pwd='')
	{
		if(!trim($email) || !trim($pwd))	return $this->Utils->result(0,2,"ERROR_PARAM");
		if(strlen($email)<4 || strlen($pwd)<6)	return $this->Utils->result(0,2,"ERROR_PARAM");
		if(!($email==$this->Utils->email && $pwd==$this->Utils->pwd))
			$this->Utils->result([],1,"ERROR USER or PWD");
		 	
		$this->Utils->result( $this->Utils->def_admin());
		 

	}
	
	
	public function logout($token)
	{
		return $this->Utils->result([]);
	}
	public function token($token="")
	{
		if(strlen($token)!=32 )	return $this->Utils->result(0,2,"ERROR_PARAM");
		if( $token!= $this->Utils->token)	
			$this->Utils->result($res,100,"INVALID_TOKEN");
		
		$this->Utils->result( $this->Utils->def_admin());
	}
	
}	
