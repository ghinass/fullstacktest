<?php
/**
 * Created by PhpStorm.
 * User: Basel
 * Date: 22/06/2019
 * Time: 01:42 م
 */

class Students  extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        header('Access-Control-Allow-Origin: *');
        $this->load->model('utils/Utils');
        $this->load->model('db/Students_model','Students');
        $this->admin=$this->Utils->admin_token();
        $this->lists=['status'=>array(1=>'Successful',2=>'Failed')];
    }
    public function index()
    {
        $rows=$this->Students->rows();
        $rows['lists']=$this->lists ;
        $this->Utils->Result( 	$rows	 );
    }

    public function view($id=0)
    {
        $res=array('row'=>$this->Students->row($id));
        $res['lists']=$this->lists ;
        $this->Utils->Result( 	$res	 );
    }
    public function add()
    {
        $data=$this->input->post('data');
        unset($data['id']);
        if(!$data['f_name']) $this->Utils->Result(0,1,'ERROR_NAME');
        if(!$data['l_name']) $this->Utils->Result(0,1,'ERROR_NAME');
        $data['created_by']=$this->admin->id;
        $data['created_date']=date('Y-m-d H:i:s');
        $res=$this->Students->insert($data);
        $this->Utils->Result(array('insert_id'=>$res),$res==0);
    }
    public function edit($id)
    {
        $data=$this->input->post('data');
        unset($data['id']);
        $data['updated_by']=$this->admin->id;
        $data['updated_date']=date('Y-m-d H:i:s');
        $res=$this->Students->update($data,array('id'=>intval($id)));
        $this->Utils->Result(array('affected'=>$res),$res==0);
    }




}
