<?php
class Centers extends CI_Controller {
	public function __construct()
	{
			parent::__construct();
			header('Access-Control-Allow-Origin: *');
			$this->load->model('utils/Utils');
			$this->load->model('db/Centers_model','Centers');
			$this->admin=$this->Utils->admin_token();
			$this->lists=['status'=>array(1=>'Active',2=>'Inactive')];
	}
	public function index()
	{
		$rows=$this->Centers->rows();
		$rows['lists']=$this->lists ;
		$this->Utils->Result( 	$rows	 );
	}
	
	public function view($id=0)
	{
		$res=array('row'=>$this->Centers->row($id));
		$res['lists']=$this->lists ;
		$this->Utils->Result( 	$res	 );
	}
	public function add()
	{
		$data=$this->input->post('data');
		unset($data['id']);
		if(!$data['name']) $this->Utils->Result(0,1,'ERROR_NAME');
		$data['created_by']=$this->admin->id;
		$data['created_date']=date('Y-m-d H:i:s');
		$res=$this->Centers->insert($data);
		$this->Utils->Result(array('insert_id'=>$res),$res==0);
	}
	public function edit($id)
	{
		$data=$this->input->post('data');
		unset($data['id']);
		$data['updated_by']=$this->admin->id;
		$data['updated_date']=date('Y-m-d H:i:s');
		$res=$this->Centers->update($data,array('id'=>intval($id)));
		$this->Utils->Result(array('affected'=>$res),$res==0);
	}
	 
	
}	
