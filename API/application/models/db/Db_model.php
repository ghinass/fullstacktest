<?php
class Db_Model extends CI_Model {
	 
	var $table='abstract';
	public function __construct()
	{
			parent::__construct();
			 
	}
	 
	 
	 
	public function rows($cond=array(),$select='*',$table=false)
	{ 
		if(!$table)$table=$this->table;
		$query=$this->db->select($select)->where($cond)->get($table);
		$rows=array();
		foreach ($query->result() as $row) $rows[ ]= $row  ;
		return array('rows'=>$rows,'num_rows'=>$query->num_rows());
	}
	 
	public function row($cond=array(),$select='*',$table=false)
	{ 
		if(!$table)$table=$this->table;
		if(is_numeric ($cond))$cond=array('id'=>intval($cond));
		$query=$this->db->select($select)->where($cond)->get($table);
		return $query->row();
	}
	public function insert($data=array(),$table=false)
	{ 
		if(!$table)$table=$this->table;
		$this->db->insert($table, $data); 
		return $this->db->insert_id();
	}
	public function update($data=array(),$where=array(),$table=false)
	{ 
		if(!$table)$table=$this->table;  
		if(is_int($where))
			$where=array('id'=>$where);
		$this->db->update($table, $data,$where); 
		return $this->db->affected_rows();
	}
	public function delete($cond=array(),$table=false)
	{ 
		if(!$table)$table=$this->table;
		if(is_numeric ($cond))$cond=array('id'=>intval($cond));
		$this->db->delete($table,$cond); 
		return 1;
	}	
	
	 
}