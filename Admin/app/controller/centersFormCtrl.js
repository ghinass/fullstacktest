app.controller('centersFormCtrl',function($scope ,$rootScope,$routeParams,centersModel){


	$rootScope.check();
	 
	$scope.get_row=function(id)
	{
		$scope.loading=true;$scope.row={id:id};
		centersModel.row(id).then(function(res){ 
			$scope.row=res.row||{};
			$scope.lists=res.lists;
		})
		.catch(function(error){$scope.error=error})
		.finally(function(){$scope.loading=false;})
	}
 
 
	$scope.submit=function()
	{ 
		$scope.loading=true;
		$scope.error='';
		$scope.message='';
		centersModel.save($scope.row).then(function(res){
			$scope.message='Data Saved' ;
			if(res.insert_id!=undefined && Number(res.insert_id)>0)
				$scope.get_row(Number(res.insert_id));
			
		})
		.catch(function(error){$scope.error=error})
		.finally(function(){$scope.loading=false;})
	}

	var centerId=Number($routeParams.ID);
	$scope.get_row(centerId);
	
});