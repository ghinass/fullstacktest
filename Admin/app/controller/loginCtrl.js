app.controller('loginCtrl',function($scope,$rootScope,authenticateService,$timeout){

	

	 
	$scope.login=function()
	{
		 
		$scope.error='';
		$scope.loading=true;
		authenticateService.login($scope.email,$scope.pwd).then(function(data){
			
			 $timeout(function(){$scope.href('main')},0);
		}).catch(function(error){
		console.info(error)
		$scope.error=error.msg;
		}).finally(function(){$scope.loading=0;})
		
	}
});