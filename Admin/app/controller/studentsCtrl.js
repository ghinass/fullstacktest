app.controller('studentsCtrl',function($scope ,$rootScope,studentsModel){

    $rootScope.check();

    $scope.get_rows=function( )
    {
        $scope.loading=true;
        $scope.error='';
        $scope.message='';
        studentsModel.rows().then(function(res){
            $scope.rows=res.rows;
            $scope.lists=res.lists;
        })
            .catch(function(error){$scope.error=error})
            .finally(function(){$scope.loading=false;})
    }
    $scope.get_rows();


});
