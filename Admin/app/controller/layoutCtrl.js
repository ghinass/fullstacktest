app.controller('layoutCtrl',function($scope,$rootScope,authenticateService,$timeout ){

	 
	$rootScope.$watch('account',function(nVal,oVal){
		
		if(!$rootScope.account)
		{ 
			$scope.nav_template=''; 
			return;
		}
		$scope.admin_name=$rootScope.account.name;
		$scope.nav_template='pages/inc/nav.html';
	}); 
	$scope.logoff=function()
	{
		
		authenticateService.logoff().then(function(){
			$timeout(function(){$scope.href('init')},0);
		});
		
	}
 
});