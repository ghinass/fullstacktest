app.controller('centersCtrl',function($scope ,$rootScope,centersModel){

	$rootScope.check();
	 
	$scope.get_rows=function( )
	{
		$scope.loading=true;
		$scope.error='';
		$scope.message='';
		centersModel.rows().then(function(res){
			$scope.rows=res.rows;
			$scope.lists=res.lists;
		})
		.catch(function(error){$scope.error=error})
		.finally(function(){$scope.loading=false;})
	}
	$scope.get_rows();
	
	
});