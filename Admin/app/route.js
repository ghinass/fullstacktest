app.config(function($routeProvider) {
        $routeProvider
			.when('/init', {
                templateUrl : 'pages/init.html',controller  : 'initCtrl'
            })
			.when('/login', {
                templateUrl : 'pages/login.html',controller  : 'loginCtrl'
            })
			.when('/main', {
                templateUrl : 'pages/main.html',controller  : 'mainCtrl'
            })
			.when('/centers', { templateUrl: 'pages/centers/list.html', controller: 'centersCtrl' })
			.when('/centers/:ID', { templateUrl: 'pages/centers/form.html', controller: 'centersFormCtrl' })
            .when('/students', { templateUrl: 'pages/students/list.html', controller: 'studentsCtrl' })
            .when('/students/:ID', { templateUrl: 'pages/students/form.html', controller: 'studentsFormCtrl' })

			.otherwise({redirectTo: '/init'});
			 
    });
	 
 
