app.factory('centersModel',function($q,dataService){
	var M={};
	 
	M.rows=function()
	{
		var promise=$q.defer()
		dataService.rows('centers').then(promise.resolve).catch(promise.reject)
		return promise.promise;
	}
	M.row=function(id)
	{
		id=Number(id)||0;
		var promise=$q.defer()
		dataService.row('centers/view',id).then(function(res){
			if(!res.row)res.row={id:0,status:"1"};
			promise.resolve(res)
		}).catch(promise.reject)
		return promise.promise;
	}
	M.save=function(row)
	{ 
		var id=Number(row.id)||0;
		if(id)return M.edit(row,id);
		return M.add(row);
		
	}
	M.add=function(row)
	{
		var promise=$q.defer()
		dataService.add('centers/add',row).then(function(res){ 
			promise.resolve(res)
		}).catch(promise.reject)
		return promise.promise;
	}
	M.edit=function(row,id)
	{
		var promise=$q.defer()
		dataService.edit('centers/edit',row,id).then(function(res){
			promise.resolve(res)
		}).catch(promise.reject)
		return promise.promise;
	}
	return M;
})