app.factory('authenticateService',function($q,$http,$rootScope,dataService){
	var H={};
	 
	H.login=function(email,pwd)
	{
		var promise=$q.defer();
		dataService.login(email,pwd).then(function(res){
			
			H.setToken(res.token);
			 
			$rootScope.account=res;
			 
			promise.resolve(res);
		})
		.catch(promise.reject);
		return promise.promise;
	}
	 
	 
	H.setToken=function(token)
	{
		dataService.token= token;
	}
	H.setDataToken=function(token)
	{
		var promise=$q.defer();
		dataService.check_token(token).then(function(res){
			dataService.token=res.token;
			H.setToken(res.token);
			$rootScope.account=res;
			 
			promise.resolve(res);
		}).catch(function(res){
			H.setToken("");
			dataService.token=false;
			promise.reject(res);
		}) ;
		return promise.promise;
	}
	H.logoff=function()
	{
		var promise=$q.defer();
		dataService.logoff(dataService.token)
		.then(promise.resolve)
		.catch(promise.reject)
		.finally(function(){
			$rootScope.account=false;
			H.setToken("");
			dataService.token=false;
		}) ;
		return promise.promise;
		
		
	}
	 
	return H;
})