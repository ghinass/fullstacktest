app.factory('dataService',function($q,$http,$rootScope,$httpParamSerializerJQLike,$timeout ){
	$http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded; charset=utf-8";
	 
	var H={token:false};
	var server=$rootScope.server;
	H.login=function(email,pwd)
	{
		var promise=$q.defer();
		$http.get(server+'admin/login/authenticate/'+email+'/'+pwd)
		.then(function(res){
			if(res.data.error==undefined || res.data.error>0)return promise.reject(res.data);
			
			promise.resolve(res.data.data)
		},promise.reject)
		 
		return promise.promise;
	}
	H.check_token=function(token)
	{
		var promise=$q.defer();
		$http.get(server+'admin/login/token/'+token)
		.then(function(res){
			if(res.data.error==undefined || res.data.error>0)return promise.reject(res.data);
			
			promise.resolve(res.data.data)
		},promise.reject);
		return promise.promise;
	}
	H.logoff=function(token)
	{
		var promise=$q.defer();
		$http.get(server+'admin/login/logout/'+token)
		.then(function(res){
			if(res.data.error==undefined || res.data.error>0)return promise.reject(res.data);
			
			promise.resolve(res.data.data)
		},promise.reject);
		return promise.promise;
	}
	
	H.rows=function(folder)
	{
		return H.post('admin/'+folder);
	}
	H.row=function(folder,id)
	{
		return H.post( 'admin/'+folder+'/'+id);
	}
	H.add=function(path,data)
	{
		return H.post( 'admin/'+path,data);
	}
	H.edit=function(path,data,id)
	{
		return H.post( 'admin/'+path+'/'+id,data);
	}
	function asUrl(data)
	{
		var parts=[];
		$.each(data,function(k,v){parts.push([k,v].join('='))});
		return parts.join('&');
	}
	H.post=function(url,data)
	{
		var promise=$q.defer();
		data={data:data||{}};
		data.token=H.token;
		
		$http.post(server+url,$httpParamSerializerJQLike(data))
		.then(function(res){
			if(res.data.error==undefined || res.data.error>0)
			{
				if(res.data.msg=='INVALID_TOKEN')
				$timeout(function(){$rootScope.account=false;$scope.href('login')},0);
				return promise.reject(res.data);
			}
			promise.resolve(res.data.data)
		},promise.reject)
		 
		return promise.promise;
	}
	return H;
})